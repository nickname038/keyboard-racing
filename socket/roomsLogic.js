import * as config from "./config";
import { texts } from "./../data";

const rooms = new Map();

export class RoomLogicHandler {
  constructor(io, socket, username) {
    this.io = io;
    this.socket = socket;
    this.username = username;
    this.room = null;
    this.results = [];
  }

  getRooms = () => {
    const allRooms = Array.from(rooms.values());
    const availableRooms = allRooms.filter((room) => room.isAvailable);
    return availableRooms;
  };

  onCreateRoom = (newRoomName) => {
    if (!rooms.has(newRoomName)) {
      const newRoom = {
        name: newRoomName,
        userCount: 1,
        isAvailable: true,
        isGameInProgress: false,
        users: [{ name: this.username, isReady: false, progress: 0 }],
      };
      rooms.set(newRoomName, newRoom);
      this.room = newRoom;
      this.socket.broadcast.emit("UPDATE_ROOMS", this.getRooms());
      this.socket.join(newRoomName);
      this.socket.emit("UPDATE_PLAY_ROOM", this.room);
    } else {
      this.socket.emit("CREATE_ROOM_ERROR");
    }
  };

  onJoinRoom = (roomName) => {
    this.socket.join(roomName);
    const currentRoomState = rooms.get(roomName);
    currentRoomState.userCount++;

    currentRoomState.users.push({
      name: this.username,
      isReady: false,
      progress: 0,
    });
    rooms.set(roomName, currentRoomState);
    this.room = currentRoomState;
    this.io.to(roomName).emit("UPDATE_PLAY_ROOM", this.room);
    if (this.room.userCount >= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      this.room.isAvailable = false;
      rooms.set(roomName, this.room);
    }
    this.socket.broadcast.emit("UPDATE_ROOMS", this.getRooms());
  };

  onLeaveRoom = () => {
    if (this.room) {
      const { name: roomName } = this.room;
      this.socket.leave(roomName);
      const userObj = this.getUserByName(this.username);
      const index = this.room.users.indexOf(userObj);
      this.room.users.splice(index, 1);
      this.room.userCount--;
      if (!this.room.isGameInProgress) {
        this.room.isAvailable = true;
      }
      rooms.set(roomName, this.room);
      if (this.room.userCount <= 0) {
        rooms.delete(roomName);
      } else {
        if (!this.room.isGameInProgress) {
          this.checkAllUsersReady();
        }
      }

      if (this.room.isGameInProgress) {
        this.checkAllUsersFinished();
      }

      this.io.to(roomName).emit("UPDATE_PLAY_ROOM", this.room);
      this.io.of("").emit("UPDATE_ROOMS", this.getRooms());
    }
  };

  getUserByName = (name) => {
    let searchedUser;
    for (const user of this.room.users) {
      if (user.name === name) {
        searchedUser = user;
        break;
      }
    }
    return searchedUser;
  };

  checkAllUsersReady = () => {
    if (this.room.users.every((user) => user.isReady)) {
      const textId = this.getRandomTextId();
      this.room.isGameInProgress = true;
      this.room.isAvailable = false;
      this.socket.broadcast.emit("UPDATE_ROOMS", this.getRooms());
      this.io.to(this.room.name).emit("START_TIMER", {
        time: config.SECONDS_TIMER_BEFORE_START_GAME,
        textId,
        gameTime: config.SECONDS_FOR_GAME,
      });

      const gameDuration =
        (config.SECONDS_TIMER_BEFORE_START_GAME + config.SECONDS_FOR_GAME) *
        1000;

      this.timerId = setTimeout(() => {
        this.onEndGame();
      }, gameDuration);
    }
  };

  checkAllUsersFinished = () => {
    if (this.room.users.every((user) => user.progress === 100)) {
      this.onEndGame();
      clearTimeout(this.timerId);
    }
  };

  getRandomTextId = () => {
    const textListSize = texts.length - 1;
    const index = Math.floor(Math.random() * (textListSize + 1));
    return index;
  };

  onReady = () => {
    const { name: roomName } = this.room;
    const userObj = this.getUserByName(this.username);
    userObj.isReady = !userObj.isReady;
    if (userObj.isReady) {
      this.checkAllUsersReady();
    }
    this.io.to(roomName).emit("UPDATE_PLAY_ROOM", this.room);
  };

  onProgressChange = (progress) => {
    const userObj = this.getUserByName(this.username);
    userObj.progress = progress;
    if (progress === 100) {
      userObj.timeFinished = Date.now();
      this.checkAllUsersFinished();
    }
    this.io.to(this.room.name).emit("UPDATE_PLAY_ROOM", this.room);
  };

  onEndGame = () => {
    const finishedUsers = [];
    const notFinishedUsers = [];

    this.room.users.forEach((user) => {
      if (user.timeFinished) {
        finishedUsers.push(user);
      } else {
        notFinishedUsers.push(user);
      }
    });

    finishedUsers.sort(
      (user1, user2) => user1.timeFinished - user2.timeFinished
    );
    notFinishedUsers.sort((user1, user2) => user2.progress - user1.progress);

    const results = finishedUsers.concat(notFinishedUsers);

    this.io.to(this.room.name).emit(
      "RESULTS",
      results.map((user) => user.name)
    );

    this.room.users.forEach((user) => {
      user.isReady = false;
      user.progress = 0;
      user.timeFinished = undefined;
    });

    this.io.to(this.room.name).emit("UPDATE_PLAY_ROOM", this.room);

    this.room.isGameInProgress = false;
    if (this.room.userCount < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      this.room.isAvailable = true;
    }

    this.io.of("").emit("UPDATE_ROOMS", this.getRooms());
  };
}
