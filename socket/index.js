import { RoomLogicHandler } from "./roomsLogic";

const userNames = new Set();

export default (io) => {
  io.of("").on("connection", (socket) => {
    const username = socket.handshake.query.username;
    const roomLogicHandler = new RoomLogicHandler(io, socket, username);

    if (userNames.has(username)) {
      socket.emit("AUTHORISATION_ERROR");
    } else {
      userNames.add(username);
      socket.emit("UPDATE_ROOMS", roomLogicHandler.getRooms());
    }

    socket.on("CREATE_ROOM", roomLogicHandler.onCreateRoom);
    socket.on("JOIN_ROOM", roomLogicHandler.onJoinRoom);
    socket.on("LEAVE_ROOM", roomLogicHandler.onLeaveRoom);
    socket.on("READY", roomLogicHandler.onReady);
    socket.on("PROGRESS_CHANGED", roomLogicHandler.onProgressChange);

    socket.on("DELETE_USER", (username) => {
      userNames.delete(username);
    });

    socket.on("disconnect", roomLogicHandler.onLeaveRoom);
  });
};
