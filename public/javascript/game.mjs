import { RoomsHandler } from "./roomsHandler.mjs";
import { GameHandler } from "./gameHandler.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });
const roomsHandler = new RoomsHandler(socket, username);
const gameHandler = new GameHandler(socket, username);

socket.on("AUTHORISATION_ERROR", roomsHandler.onAuthorisationError);
socket.on("UPDATE_ROOMS", roomsHandler.onUpdateRooms);
socket.on("CREATE_ROOM_ERROR", roomsHandler.onCreateRoomError);
socket.on("UPDATE_PLAY_ROOM", gameHandler.onUpdatePlayRoom);
socket.on("START_TIMER", gameHandler.onStartTimer);
socket.on("RESULTS", gameHandler.onResults);

window.addEventListener("unload", () => {
  socket.emit("DELETE_USER", username);
});
